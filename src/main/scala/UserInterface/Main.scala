
import Parsing.Parsing.Parse
import Storing_quering.Quering.query
import Storing_quering.reports.Report


object Main {


  def main(args: Array[String]): Unit = {
    Parse()
    println("Voulez-vous : faire une requete(1) ou obtenir un rapport(2)")
    scala.io.StdIn.readInt() match {
      case 1 => query()
      case 2 => Report()
    }

  }
}
