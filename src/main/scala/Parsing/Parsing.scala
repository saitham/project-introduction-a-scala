package Parsing

import Storing_quering.Quering.air
import Storing_quering.Quering.run
import Storing_quering.Quering.countries

import Storing_quering.{Airport, Countries, Runway}

import scala.io.Source
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Parsing {

  /**
   * Lit les donner d'un fichier csv
   *
   * @param fileName
   * @return les donnees
   */
  def readData(fileName: String): Vector[String] = {
    for {
      line <- Source.fromFile(fileName).getLines().toVector
    } yield line
  }


  def parseCsvLineAirport(line: String): Option[Airport] = {
    line.split(",", -1).map(_.trim).toVector match {
      case Vector(id, ident, _type, name, atitude_deg, longitude_deg, elevation_ft, continent, iso_country, iso_region,
      municipality, scheduled_service, gps_code, iata_code, local_code, home_link, wikipedia_link, keywords) =>
        Some(Airport(id, ident, _type, name, atitude_deg, longitude_deg, elevation_ft, continent, iso_country,
          iso_region, municipality, scheduled_service, gps_code, iata_code, local_code, home_link, wikipedia_link,
          keywords))
      case _ => println(s"WARNING UNKNOWN DATA FORMAT FOR LINE: $line")
        None
    }
  }


  def parseCsvLineRunway(line: String): Option[Runway] = {
    line.split(",", -1).toVector match {
      case Vector(id, airport_ref, airport_ident, length_ft, width_ft, surface, lighted, closed, le_ident,
      le_latitude_deg, le_longitude_deg, le_elevation_ft, le_heading_degT, le_displaced_threshold_ft, he_ident,
      he_latitude_deg, he_longitude_deg, he_elevation_ft, he_heading_degT, he_displaced_threshold_ft) =>
        Some(Runway(id, airport_ref, airport_ident, length_ft, width_ft, surface, lighted, closed, le_ident,
          le_latitude_deg, le_longitude_deg, le_elevation_ft, le_heading_degT, le_displaced_threshold_ft, he_ident,
          he_latitude_deg, he_longitude_deg, he_elevation_ft, he_heading_degT, he_displaced_threshold_ft))
      case _ => println("Run")
        None
    }
  }


  def parseCsvLineContries(line: String): Option[Countries] = {
    line.split(",", -1).toVector match {
      case Vector(id, code, name, continent, wikipedia_link, keywords) =>
        Some(Countries(id, code, name, continent, wikipedia_link,
          keywords))
      case _ => println("Country")
        None
    }
  }

  def exist[T](l: ListBuffer[T], x: Option[T]): Unit = {
    if (!x.isEmpty){
      l.append(x.get)
    }
  }

  def Parse(): Unit ={
    val list_air = readData("airports.csv").map(x => parseCsvLineAirport(x))
    list_air.foreach(x => exist(air, x))
    val list_run = readData("runways.csv").map(x => parseCsvLineRunway(x))
    list_run.foreach(x => exist(run, x))
    val list_count = readData("countries.csv").map(x => parseCsvLineContries(x))
    list_count.foreach(x => exist(countries, x))
  }

}