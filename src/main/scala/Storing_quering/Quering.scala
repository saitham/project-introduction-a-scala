package Storing_quering



import scala.collection.{immutable, mutable}
import scala.collection.mutable.ListBuffer
object Quering {

  val air = new ListBuffer[Airport]()
  val run = new ListBuffer[Runway]()
  val countries = new ListBuffer[Countries]()

  def is_code(name: String, found: StringBuilder, x: Countries): Unit = {
    if (x.name.equals(name))
      found.append(x.Code)
  }

  def match_country_name(name: String): Option[String] = {
    val found = new StringBuilder()
    countries.foreach(x => is_code(name, found, x))
    Some(found.toString())
  }

  def is_in_airport2(x: Runway, airport: String): Unit = {
    if (x.airport_ref.equals(airport)){
      println(x.airport_ident + " id :" + x.id)
    }
  }

  def is_in_airport(airport: String): Unit = {
    println("Les pistes de l'aeroport:")
    run.foreach(x => is_in_airport2(x, airport))
    println()
  }



  def is_in_country(x: Airport, code: String): Unit = {
    if (x.iso_country.equals(code)) {
      println("L'aeroport :" + x.name)
      is_in_airport(x.id.trim);
    }
  }

  def all_airport(code: Option[String]): Unit = {
    code match {
      case None => println("Le code ne correspond a aucun pays")
      case default => air.foreach(x => is_in_country(x, code.get))
    }
  }

  def query(): Unit = {
    scala.io.StdIn.readLine("Entrez le nom ou code d'un pays\n") match {
      case x if x.length() > 2 => all_airport(match_country_name(x.trim))
      case x => all_airport(Some(x))
    }
  }


}
