package Storing_quering

import Storing_quering.Quering.{air, countries, run}

import scala.collection.{immutable, mutable}
import scala.collection.mutable.ListBuffer

object reports {

  def init_max_min(countries_count: mutable.Map[String, Int], x: Countries): Unit = {
    countries_count += (x.name -> air.count(p => p.iso_country.equals(x.Code)))
  }


  def max_min(): Unit = {
    val countries_count = mutable.Map[String, Int]()
    countries.foreach(x => init_max_min(countries_count, x))
    val first = immutable.ListMap(countries_count.toSeq.sortWith(_._2 < _._2):_*).take(10)
    val last = immutable.ListMap(countries_count.toSeq.sortWith(_._2 > _._2):_*).take(10)
    println("Les 10 pays ayant le plus d'aeroports: ")
    last.foreach(x => println(x._1 + "(" + x._2 + ")"))
    println("Les 10 pays ayant le moins d'aeroports: ")
    first.foreach(x => println(x._1 + "(" + x._2 + ")"))
  }


  def add_type_runways_to_list(list: ListBuffer[String], seq: Seq[Airport], x: Runway): Unit = {
    if (seq.count(p => p.id == x.airport_ref) != 0 && !list.contains(x.airport_ident))
      list.addOne(x.airport_ident)
  }

  def List_of_runway_type(seq: Seq[Airport]): ListBuffer[String] = {
    val list = new ListBuffer[String]
    run.foreach(x => add_type_runways_to_list(list, seq, x))
    list
  }

  def add_all_type_runways_to_list(x: (String, Seq[Airport]), List: mutable.Map[String, ListBuffer[String]]): Unit = {
    List.addOne(x._1, List_of_runway_type(x._2))
  }

  def All_List_of_runway_type(list: mutable.Map[String, ListBuffer[String]]): Unit = {
    air.toSeq.groupBy(_.iso_country).foreach(x => add_all_type_runways_to_list(x, list))
  }


  def print_all(x: (String, ListBuffer[String])): Unit = {
    val country = countries.filter(p => p.Code.equals(x._1))
    if (country.size != 0) {
      println("Pays:" + country(0).name)
      println("Liste des types de piste:")
      x._2.foreach(y => println(y))
    }
  }

  def runways_by_countries(): Unit = {
    val runway_type = mutable.Map[String, ListBuffer[String]]()
    All_List_of_runway_type(runway_type)
    runway_type.foreach(x => print_all(x))
  }

  def communs_runway_latitude(): Unit = {
    val sorted = immutable.ListMap(run.toSeq.groupBy(_.le_ident).toSeq.sortWith(_._2.size > _._2.size):_*).take(10)
    println("Les 10 plus communes pistes par ordre decroissant")
    sorted.foreach(x => println(x._1))
  }

  def Report(): Unit = {

    println("Voulez-vous : Les 10 pays qui ont le plus et le moins d'aeroports(1) \n" +
      "Les differents types de pistes par pays(2)\n" +
      "Les 10 plus communnes latitudes de piste(3)")
    scala.io.StdIn.readInt() match {
      case 1 => max_min()
      case 2 => runways_by_countries()
      case 3 => communs_runway_latitude()
    }
  }

}
